D7 Ported Functions for D8/9
=======================================

Let's face it, there are a lot of things in Drupal 7 which were
easier to do than in Drupal 8/9.  We had single function calls
which did the trick, which now in D8 can take multiple lines of
somewhat complex code.

With this module (simply d7d8 for short), those functions are
made available to Drupal 8/9 developers as new functions.  
You call call your old favorite functions (where available) 
by just added d7d8_ in front.


Example:

- arg() has been depreciated in Drupal 8.
- d7d8_arg() can now be used as a drop-in replacement in Drupal 8/9.


Check the d7d8.module file itself for the functions you can now use in Drupal 8/9.

See the TODO: items in the module file for functions needed.